package com.example.browserapp

import android.content.res.Configuration
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {
    private lateinit var searchButton: Button
    private lateinit var webView: WebView
    private lateinit var searchInput: TextInputEditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        searchButton = findViewById(R.id.search_button)
        webView = findViewById(R.id.web_view)
        searchInput = findViewById(R.id.search_input_edit_text)

        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url.toString())
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                val title = if (view?.title.isNullOrEmpty()) {
                    getString(R.string.app_name)
                } else {
                    view?.title.toString()
                }
                supportActionBar?.title = title
            }
        }

        searchButton.setOnClickListener {
            webView.loadUrl(searchInput.text.toString())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.color_blue -> {
            searchButton.setBackgroundColor(Color.BLUE)
            true
        }

        R.id.color_green -> {
            searchButton.setBackgroundColor(Color.GREEN)
            true
        }

        R.id.color_violet -> {
            searchButton.setBackgroundColor(Color.MAGENTA)
            true
        }

        R.id.color_brown -> {
            searchButton.setBackgroundColor(Color.parseColor("#8B4513"))
            true
        }

        R.id.color_yellow -> {
            searchButton.setBackgroundColor(Color.YELLOW)
            true
        }

        R.id.color_orange -> {
            searchButton.setBackgroundColor(Color.parseColor("#FFA500"))
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_PORTRAIT)
            menuInflater.inflate(R.menu.menu_portrait, menu)
        else menuInflater.inflate(R.menu.menu_landscape, menu)
        return super.onCreateOptionsMenu(menu)
    }
}
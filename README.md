# Browser App

- The app should have an option of entering a web link in edit text and by tapping on the search button (a separate view), the page from the web link should be loaded.
- The app should have an action bar with the title of the current WebView page. If it is empty or not loaded, it should still show the app name "Browser app".
- The action bar should have the option of changing the color of the button. The following options are allowed: blue, green, violet. Each option is a []separate menu option in the action bar. Blue is the default option.
- The search button should have the title "Search" and the text should have a floating hint "Web link".
- Finally, override the menu for a landscape layout. The following colors should be used for landscape layouts: brown, yellow, orange. Additionally, update menu titles using appropriate colors.
